package org.example.abbtech.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;
@Entity
@Table(name = "Notification",schema="public")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Notification{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "UUID")
    private UUID id;
    @Column(name = "order_id", columnDefinition = "UUID")
    private UUID order_id;
    @Column(name = "user_id", columnDefinition = "UUID")
    private UUID user_id;
    @Column(name = "email")
    private String email;
    @Column(name = "message")
    private String message;
}
