package org.example.abbtech.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("")
@RequiredArgsConstructor
@Validated
public class NotificationController {
    @GetMapping()
    public String sendNotification(@RequestParam(value = "bookId", required = false) UUID bookId) {
        System.out.println("send notification for " + bookId);
        return "notification send success";
    }

}
