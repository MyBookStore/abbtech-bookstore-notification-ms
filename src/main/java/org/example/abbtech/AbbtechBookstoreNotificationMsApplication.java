package org.example.abbtech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients

public class AbbtechBookstoreNotificationMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AbbtechBookstoreNotificationMsApplication.class, args);
    }

}
